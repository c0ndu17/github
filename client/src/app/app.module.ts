import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { SpinnerModule } from 'angular-spinners';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { AppComponent } from './app.component';

/**
 * Containers
 */
import { HomeComponent } from './containers/home/home.component';

/**
 * Components
 */
import { IssueComponent } from './components/issue/issue.component';
import { IssueDetailComponent } from './components/issue-detail/issue-detail.component';
import { OpenIssuesComponent } from './components/open-issues/open-issues.component';
import { RepositoryDetailComponent } from './components/repository-detail/repository-detail.component';
import { RepoCardComponent } from './components/repo-card/repo-card.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';

/*
 *  Services
 */
import { GithubService } from './services/github/github.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    IssueComponent,
    IssueDetailComponent,
    RepositoryDetailComponent,
    RepoCardComponent,
    OpenIssuesComponent,
    ToolbarComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
    ]),
    InfiniteScrollModule,
    SpinnerModule,
  ],
  providers: [
    GithubService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
