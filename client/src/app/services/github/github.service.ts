import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import * as _ from 'lodash';

const API_ROOT = 'https://api.github.com';

@Injectable()
export class GithubService {
  /*
   *  Repository store, Grouped by repo id
   */
  private repositoryStore: BehaviorSubject<any> = new BehaviorSubject<any>({});
  private loadingRepos: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  /*
   *  Issue store, Grouped by repo id
   */
  private issueStore: BehaviorSubject<any> = new BehaviorSubject<any>({});
  private loadingIssues: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  /*
   *  Active repo, holds the repo id, of the currently selected repo || null
   */
  public activeRepo: BehaviorSubject<string> = new BehaviorSubject('');

  public activeQuery = '';

  public currentRepoPage = 1;

  public currentIssuesPage = 1;

  constructor(
    public http: Http
  ) {}

  reposLoading(): BehaviorSubject<boolean> {
    return this.loadingRepos;
  }

  issuesLoading(): BehaviorSubject<boolean> {
    return this.loadingIssues;
  }

  subscribeToRepositoryStore() {
    return this.repositoryStore;
  }

  subscribeToIssueStore() {
    return this.issueStore;
  }

  subscribeToActiveRepo() {
    return this.activeRepo;
  }

  getRepoData(repoId: string) {
    return this.repositoryStore.getValue()[repoId];
  }

  getActiveRepoData() {
    const repoId = this.activeRepo.getValue();
    if (!repoId) { return null; }
    return this.getRepoData(repoId);
  }

  setActiveRepo(repoId: string) {
    this.activeRepo.next(repoId);
    this.currentIssuesPage = 1;
  }

  clearActiveRepo() {
    this.activeRepo.next('');
    this.currentIssuesPage = 1;
  }

  queryRepo(queryString: string = null) {
    if (!queryString && !this.activeQuery) { return null; }
    if (queryString === this.activeQuery) { return null; }

    if (queryString && queryString !== this.activeQuery) {
      this.activeQuery = queryString;
      this.currentRepoPage = 1;
      this.currentIssuesPage = 1;
    }
    this.loadingRepos.next(true);

    const obs = this.http.get(API_ROOT + '/search/repositories?q=' + this.activeQuery + '&page=' + this.currentRepoPage + '&sort=stars')
      .map(res => res.json());

    obs.subscribe((res) => {
      if (this.currentRepoPage > 1) {
        const tmpStore = this.repositoryStore.getValue();
        this.repositoryStore.next(
          _.merge(
            tmpStore,
            _.keyBy(res.items, 'id')
          )
        );
      } else {
        this.repositoryStore.next(_.keyBy(res.items, 'id'));
      }
      if (res.items.length === 30) {
        this.currentRepoPage++;
      }
      this.loadingRepos.next(false);
    }, (err) => {
      this.loadingRepos.next(false);
    });

    return obs;
  }

  /**
   *  Lazy loading of open issues
   */
  queryIssues(repoId: string)  {
    const reposObj = this.repositoryStore.getValue();

    if (!reposObj || !reposObj[repoId] || !reposObj[repoId]['issues_url']) { return null; }

    const issuesUrl = reposObj[repoId]['url'] + '/issues?page=' + this.currentIssuesPage;

    this.loadingIssues.next(true);
    const obs = this.http.get(issuesUrl)
      .map(res => res.json());

    obs.subscribe((res) => {
      const issuesObj = this.issueStore.getValue();
      if (this.currentIssuesPage > 1) {
        issuesObj[repoId] = _.chain(issuesObj[repoId])
          .concat(res)
          .uniqBy('id')
          .sortBy('updated_at')
          .value();
      } else {
        issuesObj[repoId] = res;
      }
      if (res.length === 30) {
        this.currentIssuesPage++;
      }
      this.issueStore.next(issuesObj);
      this.loadingIssues.next(false);
    }, (err) => {
      this.loadingIssues.next(false);
    });
    return obs;
  }
}
