import { Component, OnInit, OnDestroy } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';

/**
 * Services
 */
import { GithubService } from '../../services/github/github.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  private querySubscription: Subscription;
  public loading: BehaviorSubject<any>;
  public queryResults: Array<any> = [];
  public throttle = 300;
  public scrollDistance = 1;

  constructor(
    public githubService: GithubService
  ) { }

  ngOnInit() {
    this.loading = this.githubService.reposLoading();
    this.querySubscription = this.githubService.subscribeToRepositoryStore()
    .subscribe(
      (data) => {
        this.queryResults = _.chain(data)
        .values()
        .sortBy((repo) => {
          return -1 * repo['stargazers_count'];
        })
        .value();
      },
      (err) => {
        console.log('ERROR') ;
        console.log(err) ;
      },
      () => {
        console.log('Completed');
      }
    );
  }

  ngOnDestroy() {
    if (this.querySubscription) { this.querySubscription.unsubscribe(); }
  }

  onScrollDown() {
    const isLoading = this.githubService.reposLoading().getValue();
    if (!isLoading) {
      this.githubService.queryRepo();
    }
  }
}
