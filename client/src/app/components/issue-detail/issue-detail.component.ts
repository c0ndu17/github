import {
  Component,
  Input,
  OnInit,
} from '@angular/core';

import * as moment from 'moment';

@Component({
  selector: 'app-issue-detail',
  templateUrl: './issue-detail.component.html',
  styleUrls: ['./issue-detail.component.css']
})
export class IssueDetailComponent implements OnInit {
  @Input() issue = null;

  constructor() { }

  ngOnInit() {
  }

  formatDate(issueDate) {
    return moment(issueDate).format('dddd, MMMM Do YYYY, h:mm:ss a');
  }

}
