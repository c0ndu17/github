import {
  Component,
  OnInit
} from '@angular/core';

import { Subscription } from 'rxjs/Subscription';

import { GithubService } from '../../services/github/github.service';

import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-repository-detail',
  templateUrl: './repository-detail.component.html',
  styleUrls: ['./repository-detail.component.css']
})
export class RepositoryDetailComponent implements OnInit {
  private activeRepoSubscription: Subscription;
  public repoData: any = null;
  public viewOpenIssues = false;
  public tabStates: any = {
    forks: false,
    stars: false,
    watchers: false,
  };

  constructor(
    public githubService: GithubService
  ) { }

  ngOnInit() {
    this.activeRepoSubscription = this.githubService.subscribeToActiveRepo()
      .subscribe(
        (data: string) => {
          this.clearViewOpenIssues();
          this.repoData = this.githubService.getActiveRepoData();
        },
        (err) => {
          console.log('ERROR') ;
          console.log(err) ;
        },
        () => {
          console.log('Completed');
        }
      );
  }

  closeSidebar() {
    this.githubService.clearActiveRepo();
    this.clearViewOpenIssues();
  }

  toggleViewOpenIssues() {
    this.viewOpenIssues = !this.viewOpenIssues;
  }

  setViewOpenIssues() {
    this.viewOpenIssues = true;
  }

  clearViewOpenIssues() {
    this.viewOpenIssues = false;
  }

  setTabActive(property: string) {
    for (const tab in this.tabStates) {
      if (tab !== property) {
        this.tabStates[tab] = false;
      } else {this.tabStates[tab] = true; }
    }
  }

  getTabClasses(property: string) {
    return this.tabStates[property] ? {
      'nav-link': true,
      'active': true
    } : {
      'nav-link': true
    };

  }

  formatDate(updatedAt) {
    return moment(updatedAt).format('dddd, MMMM Do YYYY, h:mm:ss a');
  }
}
