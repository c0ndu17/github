import {
  Component,
  OnInit,
  Input,
  NgZone
} from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';

/**
 * Services
 */
import { GithubService } from '../../services/github/github.service';

@Component({
  selector: 'app-open-issues',
  templateUrl: './open-issues.component.html',
  styleUrls: ['./open-issues.component.css']
})
export class OpenIssuesComponent implements OnInit {
  @Input() repoId: string = null;

  private repoIssuesSubscription: Subscription = null;
  public repoIssues: Array<any> = [];
  public loading: BehaviorSubject<any>;
  public throttle = 300;
  public scrollDistance = 1;

  constructor(
    private zone: NgZone,
    public githubService: GithubService
  ) { }

  ngOnInit() {
    this.loading = this.githubService.issuesLoading();
    this.repoIssuesSubscription = this.githubService.subscribeToIssueStore()
    .subscribe(
      (data: string) => {
        this.zone.run(() => {
          this.repoIssues = data[this.repoId] || null;
        });
      },
      (err) => {
        console.log('ERROR') ;
        console.log(err) ;
      },
      () => {
        console.log('Completed');
      }
    );
    this.githubService.queryIssues(this.repoId);
  }

  onScrollDown () {
    const isLoading = this.githubService.issuesLoading().getValue();
    if (!isLoading) {
        this.githubService.queryIssues(this.repoId);
    }
  }
}
