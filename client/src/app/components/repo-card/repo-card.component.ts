import {
  Input,
  Component,
  OnInit
} from '@angular/core';

import { Subscription } from 'rxjs/Subscription';

import { GithubService } from '../../services/github/github.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-repo-card',
  templateUrl: './repo-card.component.html',
  styleUrls: ['./repo-card.component.css']
})
export class RepoCardComponent implements OnInit {
  @Input() repo = null;
  public imageLoaded = false;

  constructor(
    public githubService: GithubService
  ) { }

  ngOnInit() {
  }

  setActive(repoId: string) {
    console.log(repoId);
    this.githubService.setActiveRepo(repoId);
  }

  setLoaded(name) {
    this.imageLoaded = true;
  }

  showImage() {
    return this.imageLoaded ? {
    } : {
      display: 'none'
    };
  }

  stopProp() {
    event.stopPropagation();
  }
}
