import {
  Component,
  OnInit
} from '@angular/core';
import { Http } from '@angular/http';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from '@angular/forms';

import { GithubService } from '../../services/github/github.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  public searchQuery = 'nodejs';

  constructor(
    private formBuilder: FormBuilder,
    public githubService: GithubService,
  ) {}

  ngOnInit() {
  }

  searchRepos() {
    this.githubService.queryRepo(this.searchQuery);
  }
}
