#! /bin/bash

# Check that libs are installed. This is mainly for containers where the source
# code is linked in via a volume.
npm install

npm start 1>&2

